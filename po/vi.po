# Vietnamese translations for gnome-sound-recorder package
# Bản dịch Tiếng Việt dành cho gói gnome-sound-recorder.
# Copyright © 2016 GNOME i18n Project for Vietnamese.
# This file is distributed under the same license as the gnome-sound-recorder package.
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015, 2016, 2017, 2019, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-sound-recorder master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-sound-recorder/"
"issues\n"
"POT-Creation-Date: 2021-02-15 00:21+0000\n"
"PO-Revision-Date: 2021-02-19 08:01+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <gnome-vi-list@gnome.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:5
#: data/org.gnome.SoundRecorder.desktop.in.in:4 data/ui/window.ui:28
#: src/application.js:32
msgid "Sound Recorder"
msgstr "Thu âm"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:6
msgid "A simple, modern sound recorder for GNOME"
msgstr "Một ứng dụng thu âm đơn giản mà hiện đại dành cho môi trường GNOME"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:10
msgid ""
"Sound Recorder provides a simple and modern interface that provides a "
"straight-forward way to record and play audio. It allows you to do basic "
"editing, and create voice memos."
msgstr ""
"Ứng dụng thu âm đưa ra một giao diện đơn giản và hiện đại cái mà đưa ra cách "
"dễ dàng để mà ghi âm và phát lại. Nó cho phép bạn thực hiện các thao tác sửa "
"cơ bản, và tạo ghi chú bằng giọng nói."

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:15
msgid ""
"Sound Recorder automatically handles the saving process so that you do not "
"need to worry about accidentally discarding the previous recording."
msgstr ""
"Úng dụng thu âm tự động xử lý quá trình ghi lại do đó bạn không cần phải lo "
"lắng về các tai nạn loại bỏ bản ghi trước đó."

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:19
msgid "Supported audio formats:"
msgstr "Các định dạng âm thanh được hỗ trợ:"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:21
msgid "Ogg Vorbis, Opus, FLAC, MP3 and MOV"
msgstr "Ogg Vorbis, Opus, FLAC, MP3 và MOV"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:94
msgid "The GNOME Project"
msgstr "Dự án GNOME"

#: data/org.gnome.SoundRecorder.desktop.in.in:5
msgid "Record sound via the microphone and play it back"
msgstr "Thu âm thông qua micrô rồi phát lại nó"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.SoundRecorder.desktop.in.in:10
msgid "Audio;Application;Record;"
msgstr "Audio;Âm;thanh;am;Application;Record;ghi;thu;"

#: data/org.gnome.SoundRecorder.gschema.xml.in:15
msgid "Window size"
msgstr "Kích cỡ cửa sổ"

#: data/org.gnome.SoundRecorder.gschema.xml.in:16
msgid "Window size (width and height)."
msgstr "Kích thước cửa sổ (rộng và cao)."

#: data/org.gnome.SoundRecorder.gschema.xml.in:20
msgid "Window position"
msgstr "Vị trí cửa sổ"

#: data/org.gnome.SoundRecorder.gschema.xml.in:21
msgid "Window position (x and y)."
msgstr "Vị trí cửa sổ (x và y)."

#: data/org.gnome.SoundRecorder.gschema.xml.in:25
msgid "Maps media types to audio encoder preset names."
msgstr ""
"Ánh xạ các kiểu đa phương tiện thành các tên bộ giải mã âm thanh đặt trước."

#: data/org.gnome.SoundRecorder.gschema.xml.in:26
msgid ""
"Maps media types to audio encoder preset names. If there is no mapping set, "
"the default encoder settings will be used."
msgstr ""
"Ánh xạ các kiểu đa phương tiện thành các tên bộ giải mã âm thanh đặt trước. "
"Nếu không có tập hợp ánh xạ nào, các cài đặt giải mã mặc định sẽ được dùng."

#: data/org.gnome.SoundRecorder.gschema.xml.in:30
msgid "Available channels"
msgstr "Các kênh sẵn có"

#: data/org.gnome.SoundRecorder.gschema.xml.in:31
msgid ""
"Maps available channels. If there is not no mapping set, stereo channel will "
"be used by default."
msgstr ""
"Bản đồ các kênh sẵn có. Nếu không có bản đồ nào được đặt, kênh lập thể sẽ "
"được dùng theo mặc định."

#: data/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "General"
msgstr "Chung"

#: data/ui/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Record"
msgstr "Thu"

#: data/ui/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Stop Recording"
msgstr "Dừng thu"

#: data/ui/help-overlay.ui:34
msgctxt "shortcut window"
msgid "Play / Pause / Resume"
msgstr "Phát / Dừng / Tiếp tục"

#: data/ui/help-overlay.ui:42
msgctxt "shortcut window"
msgid "Delete"
msgstr "Xóa"

#: data/ui/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Mở trình đơn"

#: data/ui/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Phím tắt bàn phím"

#: data/ui/help-overlay.ui:63
msgctxt "shortcut window"
msgid "Quit"
msgstr "Thoát"

#: data/ui/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Recording"
msgstr "Thu âm"

#: data/ui/help-overlay.ui:76
msgctxt "shortcut window"
msgid "Seek Backward"
msgstr "Tua lại"

#: data/ui/help-overlay.ui:83
msgctxt "shortcut window"
msgid "Seek Forward"
msgstr "Tua đi"

#: data/ui/help-overlay.ui:90
msgctxt "shortcut window"
msgid "Rename"
msgstr "Đổi tên"

#: data/ui/help-overlay.ui:97
msgctxt "shortcut window"
msgid "Export"
msgstr "Xuất ra"

#: data/ui/recorder.ui:72
msgid "Resume Recording"
msgstr "Tiếp tục thu"

#: data/ui/recorder.ui:96
msgid "Pause Recording"
msgstr "Tạm dừng thu"

#: data/ui/recorder.ui:128
msgid "Stop Recording"
msgstr "Dừng thu"

#: data/ui/recorder.ui:157
msgid "Delete Recording"
msgstr "Xóa bản thu"

#: data/ui/row.ui:165 data/ui/row.ui:444
msgid "Export"
msgstr "Xuất ra"

#: data/ui/row.ui:189 data/ui/row.ui:440
msgid "Rename"
msgstr "Đổi tên"

#: data/ui/row.ui:241
msgid "Save"
msgstr "Lưu"

#: data/ui/row.ui:278 src/recorderWidget.js:100
msgid "Delete"
msgstr "Xóa"

#: data/ui/row.ui:313
msgid "Seek 10s Backward"
msgstr "Tua lại 10 giây"

#: data/ui/row.ui:341
msgid "Play"
msgstr "Phát"

#: data/ui/row.ui:367
msgid "Pause"
msgstr "Tạm dừng"

#: data/ui/row.ui:399
msgid "Seek 10s Forward"
msgstr "Tua đi 10 giây"

#: data/ui/window.ui:60
msgid "Record"
msgstr "Thu"

#: data/ui/window.ui:149
msgid "Add Recordings"
msgstr "Thêm bản thu"

#: data/ui/window.ui:166
msgid "Use the <b>Record</b> button to make sound recordings"
msgstr "Dùng nút <b>Thu</b> để có thể tạo bản thu âm"

#: data/ui/window.ui:237
msgid "Undo"
msgstr "Hủy bước"

#: data/ui/window.ui:254
msgid "Close"
msgstr "Đóng"

#: data/ui/window.ui:290
msgid "Preferred Format"
msgstr "Định dạng ưa dùng"

#: data/ui/window.ui:292
msgid "Vorbis"
msgstr "Vorbis"

#: data/ui/window.ui:297
msgid "Opus"
msgstr "Opus"

#: data/ui/window.ui:302
msgid "FLAC"
msgstr "FLAC"

#: data/ui/window.ui:307
msgid "MP3"
msgstr "MP3"

#: data/ui/window.ui:313
msgid "Audio Channel"
msgstr "Kênh âm thanh"

#: data/ui/window.ui:315
msgid "Stereo"
msgstr "Lập thể"

#: data/ui/window.ui:320
msgid "Mono"
msgstr "Đơn kênh"

#: data/ui/window.ui:327
msgid "_Keyboard Shortcuts"
msgstr "_Phím tắt bàn phím"

#: data/ui/window.ui:332
msgid "About Sound Recorder"
msgstr "Giới thiệu về Thu âm"

#. Translators: Replace "translator-credits" with your names, one name per line
#: src/application.js:130
msgid "translator-credits"
msgstr "Nhóm Việt hóa GNOME <gnome-vi-list@gnome.org>"

#: src/application.js:132
msgid "A Sound Recording Application for GNOME"
msgstr "Ứng dụng ghi âm cho GNOME"

#. Translators: ""Recording %d"" is the default name assigned to a file created
#. by the application (for example, "Recording 1").
#: src/recorder.js:113
#, javascript-format
msgid "Recording %d"
msgstr "Bản thu %d"

#: src/recorderWidget.js:94
msgid "Delete recording?"
msgstr "Xóa bản thu?"

#: src/recorderWidget.js:95
msgid "This recording will not be saved."
msgstr "Bản ghi này sẽ không được lưu."

#: src/recorderWidget.js:99
msgid "Resume"
msgstr "Tiếp tục lại"

#. Necessary code to move old recordings into the new location for few releases
#. FIXME: Remove by 3.40/3.42
#: src/recordingList.js:42
msgid "Recordings"
msgstr "Bản thu"

#. Translators: ""%s (Old)"" is the new name assigned to a file moved from
#. the old recordings location
#: src/recordingList.js:59
#, javascript-format
msgid "%s (Old)"
msgstr "%s (Cũ)"

#: src/row.js:71
msgid "Export Recording"
msgstr "Xuất bản thu ra"

#: src/row.js:71
msgid "_Export"
msgstr "_Xuất ra"

#: src/row.js:71
msgid "_Cancel"
msgstr "_Thôi"

#: src/utils.js:48
msgid "Yesterday"
msgstr "Hôm qua"

#: src/utils.js:50
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d ngày trước"

#: src/utils.js:52
msgid "Last week"
msgstr "Tuần trước"

#: src/utils.js:54
#, javascript-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "%d tuần trước"

#: src/utils.js:56
msgid "Last month"
msgstr "Tháng trước"

#: src/utils.js:58
#, javascript-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "%d tháng trước"

#: src/utils.js:60
msgid "Last year"
msgstr "Năm trước"

#: src/utils.js:62
#, javascript-format
msgid "%d year ago"
msgid_plural "%d years ago"
msgstr[0] "%d năm trước"

#: src/window.js:71
#, javascript-format
msgid "\"%s\" deleted"
msgstr "Đã xóa “%s”"

#~ msgid "Microphone volume level"
#~ msgstr "Mức âm lượng của micrô"

#~ msgid "Microphone volume level."
#~ msgstr "Mức âm lượng của micrô."

#~ msgid "Speaker volume level"
#~ msgstr "Âm lượng loa"

#~ msgid "Speaker volume level."
#~ msgstr "Âm lượng loa."

#~ msgid "org.gnome.SoundRecorder"
#~ msgstr "org.gnome.SoundRecorder"

#~ msgid "SoundRecorder"
#~ msgstr "BộThuÂm"

#~ msgid "Sound Recorder started"
#~ msgstr "Bộ thu âm đã khởi chạy"

#~ msgid "Info"
#~ msgstr "Thông tin"

#~ msgid "Done"
#~ msgstr "Xong"

#~ msgctxt "File Name"
#~ msgid "Name"
#~ msgstr "Tên"

#~ msgid "Source"
#~ msgstr "Nguồn"

#~ msgid "Date Modified"
#~ msgstr "Ngày sửa đổi"

#~ msgid "Date Created"
#~ msgstr "Ngày tạo"

#~ msgctxt "Media Type"
#~ msgid "Type"
#~ msgstr "Kiểu"

#~ msgid "Unknown"
#~ msgstr "Chưa biết"

#~ msgid "Preferences"
#~ msgstr "Tùy thích"

#~ msgid "Recording…"
#~ msgstr "Đang thu…"

#~ msgid "%d Recorded Sound"
#~ msgid_plural "%d Recorded Sounds"
#~ msgstr[0] "Đã thu %d bản thu âm"

#~ msgid "No Recorded Sounds"
#~ msgstr "Không có bản thu âm nào"

#~ msgid "MOV"
#~ msgstr "MOV"

#~ msgid "Load More"
#~ msgstr "Tải thêm"

#~ msgid "Default mode"
#~ msgstr "Chế độ mặc định"

#~ msgid "Volume"
#~ msgstr "Âm lượng"

#~ msgid "Microphone"
#~ msgstr "Micrô"

#~ msgid "Unable to create Recordings directory."
#~ msgstr "Không thể tạo thư mục thu."

#~ msgid "Please install the GStreamer 1.0 PulseAudio plugin."
#~ msgstr "Vui lòng cài đặt phần bổ sung GStreamer 1.0 PulseAudio."

#~ msgid "Your audio capture settings are invalid."
#~ msgstr "Các cài đặt thu âm không hợp lệ."

#~ msgid "Not all elements could be created."
#~ msgstr "Không phải tất cả các phần tử được tạo."

#~ msgid "Not all of the elements were linked."
#~ msgstr "Không phải tất cả các phần tử được liên kết."

#~ msgid "No Media Profile was set."
#~ msgstr "Không hồ sơ đa phương tiện nào được đặt."

#~ msgid ""
#~ "Unable to set the pipeline \n"
#~ " to the recording state."
#~ msgstr ""
#~ "Không thể đặt đường ống dẫn \n"
#~ " đến trạng thái thu."

#~ msgid "Clip %d"
#~ msgstr "Clíp %d"

#~ msgid "About"
#~ msgstr "Giới thiệu"

#~ msgid "%Y-%m-%d %H:%M:%S"
#~ msgstr "%H:%M:%S %d-%m-%Y"
