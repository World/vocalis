# Occitan translation for gnome-sound-recorder.
# Copyright (C) 2014 gnome-sound-recorder's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-sound-recorder package.
# Cédric Valmary <cvalmary@yahoo.fr>, 2015.
# Cédric Valmary (Tot en òc) <cvalmary@yahoo.fr>, 2015.
# Cédric Valmary (totenoc.eu) <cvalmary@yahoo.fr>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: gnome-sound-recorder master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-sound-recorder/"
"issues\n"
"POT-Creation-Date: 2022-09-06 11:53+0000\n"
"PO-Revision-Date: 2022-12-04 20:17+0100\n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: Tot En Òc\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.2.2\n"
"X-Project-Style: gnome\n"

#: data/org.gnome.SoundRecorder.desktop.in.in:4
#: data/org.gnome.SoundRecorder.metainfo.xml.in.in:5 data/ui/window.ui:10
#: src/application.js:32
msgid "Sound Recorder"
msgstr "Enregistrador de son"

#: data/org.gnome.SoundRecorder.desktop.in.in:5
msgid "Record sound via the microphone and play it back"
msgstr "Enregistrar lo son amb un microfòn e lo restituir"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.SoundRecorder.desktop.in.in:10
msgid "Audio;Application;Record;"
msgstr "Audio;àudio;aplicacion;enregistrar;son;dictafon;enregistrador;"

#: data/org.gnome.SoundRecorder.metainfo.xml.in.in:6
msgid "A simple, modern sound recorder for GNOME"
msgstr "Un enregistrador de son simple e modèrne per GNOME"

#: data/org.gnome.SoundRecorder.metainfo.xml.in.in:10
msgid ""
"Sound Recorder provides a simple and modern interface that provides a "
"straight-forward way to record and play audio. It allows you to do basic "
"editing, and create voice memos."
msgstr ""
"L'Enregistrador de son presenta una interfàcia simpla e modèrna que provesís "
"un mejan dirècte d'enregistrar e de legir de contengut àudio. Permet de "
"modificar lo son d'un biais basic e de crear de memòs vocals."

#: data/org.gnome.SoundRecorder.metainfo.xml.in.in:15
msgid ""
"Sound Recorder automatically handles the saving process so that you do not "
"need to worry about accidentally discarding the previous recording."
msgstr ""
"L'Enregistrador de son gerís automaticament lo processus d'enregistrament "
"sus disc, aquò evita de se dever tracassar d'una pèrda accidentala d'un "
"enregistrament sonòr precedent."

#: data/org.gnome.SoundRecorder.metainfo.xml.in.in:19
msgid "Supported audio formats:"
msgstr "Formats àudio preses en cargar :"

#: data/org.gnome.SoundRecorder.metainfo.xml.in.in:21
msgid "Ogg Vorbis, Opus, FLAC, MP3 and MOV"
msgstr "Ogg Vorbis, Opus, FLAC, MP3 e MOV"

#: data/org.gnome.SoundRecorder.metainfo.xml.in.in:106
msgid "The GNOME Project"
msgstr "Lo projècte GNOME"

#: data/org.gnome.SoundRecorder.gschema.xml.in:15
msgid "Window size"
msgstr "Talha de la fenèstra"

#: data/org.gnome.SoundRecorder.gschema.xml.in:16
msgid "Window size (width and height)."
msgstr "Talha de la fenèstra (largor e nautor)."

#: data/org.gnome.SoundRecorder.gschema.xml.in:20
msgid "Window position"
msgstr "Posicion de la fenèstra"

#: data/org.gnome.SoundRecorder.gschema.xml.in:21
msgid "Window position (x and y)."
msgstr "Posicion de la fenèstra (x e y)."

#: data/org.gnome.SoundRecorder.gschema.xml.in:25
msgid "Maps media types to audio encoder preset names."
msgstr ""
"Fa correspondre los tipes de mèdia e los noms predefinits de l'encodador "
"àudio."

#: data/org.gnome.SoundRecorder.gschema.xml.in:26
msgid ""
"Maps media types to audio encoder preset names. If there is no mapping set, "
"the default encoder settings will be used."
msgstr ""
"Fa correspondre los tipes de mèdia e los noms predefinits de l'encodador "
"àudio. Se i a pas de correspondéncia per un tipe de mèdia, los paramètres de "
"l'encodador per defaut son utilizats."

#: data/org.gnome.SoundRecorder.gschema.xml.in:30
msgid "Available channels"
msgstr "Canals disponibles"

#: data/org.gnome.SoundRecorder.gschema.xml.in:31
msgid ""
"Maps available channels. If there is not no mapping set, stereo channel will "
"be used by default."
msgstr ""
"Junta amb los canals disponibles. Se cap de correspondéncia es pas definida, "
"lo canal esterèo serà utilizat per defaut."

#: data/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: data/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Record"
msgstr "Enregistrar"

#: data/ui/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Stop Recording"
msgstr "Arrestar l'enregistrament"

#: data/ui/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Play / Pause / Resume"
msgstr "Lectura / Pausa / Reprendre"

#: data/ui/help-overlay.ui:34
msgctxt "shortcut window"
msgid "Delete"
msgstr "Suprimir"

#: data/ui/help-overlay.ui:40
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Dobrir lo menú"

#: data/ui/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Acorchis clavièr"

#: data/ui/help-overlay.ui:52
msgctxt "shortcut window"
msgid "Quit"
msgstr "Quitar"

#: data/ui/help-overlay.ui:59
msgctxt "shortcut window"
msgid "Recording"
msgstr "Enregistrament"

#: data/ui/help-overlay.ui:63
msgctxt "shortcut window"
msgid "Seek Backward"
msgstr "Recular"

#: data/ui/help-overlay.ui:69
msgctxt "shortcut window"
msgid "Seek Forward"
msgstr "Avançar"

#: data/ui/help-overlay.ui:75
msgctxt "shortcut window"
msgid "Rename"
msgstr "Renomenar"

#: data/ui/help-overlay.ui:81
msgctxt "shortcut window"
msgid "Export"
msgstr "Exportar"

#: data/ui/recorder.ui:43
msgid "Resume Recording"
msgstr "Reprendre l'enregistrament"

#: data/ui/recorder.ui:61
msgid "Pause Recording"
msgstr "Metre en pausa l'enregistrament"

#: data/ui/recorder.ui:78
msgid "Stop Recording"
msgstr "Arrestar l'enregistrament"

#: data/ui/recorder.ui:98
msgid "Delete Recording"
msgstr "Suprimir l'enregistrament"

#: data/ui/row.ui:109 src/recorderWidget.js:100
msgid "Delete"
msgstr "Suprimir"

#: data/ui/row.ui:129
msgid "Seek 10s Backward"
msgstr "Anar 10s enrèire"

#: data/ui/row.ui:147
msgid "Play"
msgstr "Legir"

#: data/ui/row.ui:171
msgid "Pause"
msgstr "Pausa"

#: data/ui/row.ui:194
msgid "Seek 10s Forward"
msgstr "Anar 10s en avant"

#: data/ui/row.ui:221 data/ui/row.ui:298
msgid "Export"
msgstr "Exportar"

#: data/ui/row.ui:234 data/ui/row.ui:294
msgid "Rename"
msgstr "Renomenar"

#: data/ui/row.ui:268
msgid "Save"
msgstr "Enregistrar"

#: data/ui/window.ui:28
msgid "Record"
msgstr "Enregistrar"

#: data/ui/window.ui:61
msgid "Add Recordings"
msgstr "Apondre d'enregistraments"

#: data/ui/window.ui:62
msgid "Use the <b>Record</b> button to make sound recordings"
msgstr "Utilizatz lo boton <b>Enregistrar</b> per far d'enregistraments"

#: data/ui/window.ui:98
msgid "Preferred Format"
msgstr "Format preferit"

#: data/ui/window.ui:100
msgid "Vorbis"
msgstr "Vorbis"

#: data/ui/window.ui:105
msgid "Opus"
msgstr "Opus"

#: data/ui/window.ui:110
msgid "FLAC"
msgstr "FLAC"

#: data/ui/window.ui:115
msgid "MP3"
msgstr "MP3"

#: data/ui/window.ui:121
msgid "Audio Channel"
msgstr "Canals àudio"

#: data/ui/window.ui:123
msgid "Stereo"
msgstr "Esterèo"

#: data/ui/window.ui:128
msgid "Mono"
msgstr "Mono"

#: data/ui/window.ui:135
msgid "_Keyboard Shortcuts"
msgstr "Acorchis de _clavièr"

#: data/ui/window.ui:140
msgid "About Sound Recorder"
msgstr "A prepaus de l'Enregistrador de son"

#. Translators: Replace "translator-credits" with your names, one name per line
#: src/application.js:126
msgid "translator-credits"
msgstr "Cédric Valmary (totenoc.eu) <cvalmary@yahoo.fr>."

#: src/application.js:128
msgid "A Sound Recording Application for GNOME"
msgstr "Una aplicacion d'enregistrament àudio per GNOME"

#. Translators: ""Recording %d"" is the default name assigned to a file created
#. by the application (for example, "Recording 1").
#: src/recorder.js:113
#, javascript-format
msgid "Recording %d"
msgstr "Enregistrament %d"

#: src/recorderWidget.js:94
msgid "Delete recording?"
msgstr "Suprimir l'enregistrament ?"

#: src/recorderWidget.js:95
msgid "This recording will not be saved."
msgstr "Aqueste enregistrament serà pas enregistrat."

#: src/recorderWidget.js:99
msgid "Resume"
msgstr "Reprendre"

#. Necessary code to move old recordings into the new location for few releases
#. FIXME: Remove by 3.40/3.42
#: src/recordingList.js:42
msgid "Recordings"
msgstr "Enregistraments"

#. Translators: ""%s (Old)"" is the new name assigned to a file moved from
#. the old recordings location
#: src/recordingList.js:59
#, javascript-format
msgid "%s (Old)"
msgstr "%s (Ancian)"

#: src/row.js:67
msgid "Export Recording"
msgstr "Exportar l'enregistrament"

#: src/row.js:67
msgid "_Export"
msgstr "_Exportar"

#: src/row.js:67
msgid "_Cancel"
msgstr "A_nullar"

#: src/utils.js:48
msgid "Yesterday"
msgstr "Ièr"

#: src/utils.js:50
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "fa %d jorn"
msgstr[1] "fa %d jorns"

#: src/utils.js:52
msgid "Last week"
msgstr "La setmana passada"

#: src/utils.js:54
#, javascript-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "fa %d setmana"
msgstr[1] "fa %d setmanas"

#: src/utils.js:56
msgid "Last month"
msgstr "Lo mes passat"

#: src/utils.js:58
#, javascript-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "fa %d mes"
msgstr[1] "fa %d meses"

#: src/utils.js:60
msgid "Last year"
msgstr "L'an passat"

#: src/utils.js:62
#, javascript-format
msgid "%d year ago"
msgid_plural "%d years ago"
msgstr[0] "fa a %d an"
msgstr[1] "fa %d ans"

#: src/window.js:69
#, javascript-format
msgid "\"%s\" deleted"
msgstr "« %s » suprimit"

#: src/window.js:152
msgid "Undo"
msgstr "Anullar"

#~ msgid "Close"
#~ msgstr "Tampar"

#~ msgid "Microphone volume level"
#~ msgstr "Nivèl del volum del microfòn"

#~ msgid "Microphone volume level."
#~ msgstr "Nivèl del volum del microfòn."

#~ msgid "Speaker volume level"
#~ msgstr "Nivèl del volum del naut parlaire"

#~ msgid "Speaker volume level."
#~ msgstr "Nivèl del volum del naut parlaire."

#~ msgid "SoundRecorder"
#~ msgstr "SoundRecorder"

#~ msgid "Preferences"
#~ msgstr "Preferéncias"

#~ msgid "About"
#~ msgstr "A prepaus"

#~ msgid "Sound Recorder started"
#~ msgstr "L'enregistrament a començat"

#~ msgid "Info"
#~ msgstr "Informacions"

#~ msgid "Done"
#~ msgstr "Acabat"

#~ msgctxt "File Name"
#~ msgid "Name"
#~ msgstr "Nom"

#~ msgid "Source"
#~ msgstr "Font"

#~ msgid "Date Modified"
#~ msgstr "Data de modificacion"

#~ msgid "Date Created"
#~ msgstr "Data de creacion"

#~ msgctxt "Media Type"
#~ msgid "Type"
#~ msgstr "Tipe"

#~ msgid "Recording…"
#~ msgstr "Enregistrament en cors…"

#~ msgid "%d Recorded Sound"
#~ msgid_plural "%d Recorded Sounds"
#~ msgstr[0] "%d son enregistrat"
#~ msgstr[1] "%d sons enregistrats"

#~ msgid "MOV"
#~ msgstr "MOV"

#~ msgid "Load More"
#~ msgstr "Ne cargar mai"

#~ msgid "Default mode"
#~ msgstr "Mòde per defaut"

#~ msgid "Volume"
#~ msgstr "Volum"

#~ msgid "Microphone"
#~ msgstr "Microfòn"

#~ msgid "Unable to create Recordings directory."
#~ msgstr "Impossible de crear lo repertòri dels enregistraments."

#~ msgid "Your audio capture settings are invalid."
#~ msgstr "Vòstres paramètres de captura de l'àudio son pas valids."

#~ msgid "Not all elements could be created."
#~ msgstr "Impossible de crear totes los elements."

#~ msgid "Not all of the elements were linked."
#~ msgstr "Impossible de ligar totes los elements."

#~ msgid "No Media Profile was set."
#~ msgstr "Cap de perfil del mèdia es pas estat definit."

#~ msgid ""
#~ "Unable to set the pipeline \n"
#~ " to the recording state."
#~ msgstr ""
#~ "Impossible de configurar lo pipeline \n"
#~ " a l'estat d'enregistrament."

#~ msgid "Clip %d"
#~ msgstr "Sequéncia %d"

#~ msgid "%Y-%m-%d %H:%M:%S"
#~ msgstr "%d/%m/%Y %H:%M:%S"
