# Description of the bug

Detail the issue that you're facing as much as possible. Put as much
information as you can, potentially with images showing the issue.

Vocalis version:

Operating system:

Steps to reproduce:

1. Open Vocalis
2. Change X to something else
3. ...

Expected result:

Actual result:

